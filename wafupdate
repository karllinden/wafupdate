#!/bin/sh
#
#  Copyright (C) 2015 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# This script eases updating the waf build system. Make sure wget and
# gpg are installed and run:
# $ wafupdate <version>
# This script will install or update the build system in source form as
# to ease downstream maintainence at Debian/Ubuntu. For example see [1]
# and the pages it links to. Also this makes it possible to strip
# unnecessary tools and extras from waflib.
#
# To strip of files from waflib, just add a file called .wafupdaterc to
# the toplevel source directory of the project and declare the
# WAFLIB_STRIP environment variable. When wafupdate is run it will
# source the rc file and strip accordingly.
#
# [1] https://wiki.debian.org/UnpackWaf

package=wafupdate
version=2
homepage=https://gitlab.com/karllinden/wafupdate
bugreport=https://gitlab.com/karllinden/wafupdate/issues

vcs=""

msg() {
    echo "${package}: ${@}"
}

die() {
    msg "${@}"
    exit 1
}

run() {
    msg "${@}"
    ${@}
}

xvcs() {
    if test "${vcs}" = "git"
    then
        git "${@}" || die "git ${@} failed"
    elif test -n "${vcs}"
    then
        die "unknown vcs ${vcs}"
    else
        case ${1} in
            rm)     "${@}" || die "${@} failed";;
            add)    ;;
            *)      die "unknwon vcs command ${1}";;
        esac
    fi
}

help_text="Usage: ${package} [OPTION...] <VERSION>
Install/update the waf build system.

  -v, --version  show version and exit
  -h, --help     show this help and exit

Report bugs to: ${bugreport}
${package} home page: ${homepage}"

version_text="${package} ${version}
Copyright (C) Karl Linden <karl.j.linden@gmail.com>
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."

# Target version.
target=""

while test $# -gt 0
do
    case ${1} in
        --help|-h)      echo "${help_text}"; exit 0;;
        --version|-v)   echo "${version_text}"; exit 0;;
        -*)             die unknown option ${1};;
        *)              target=${1};;
    esac
    shift
done

if test -z "${target}"
then
    echo "${help_text}"
    exit 1
fi

# Try to source the wafupdaterc file if it exists.
if test -f .wafupdaterc
then
    . ./.wafupdaterc || die "could not source .wafupdaterc"
fi

# Parse WAFLIB_STRIP_* and add to WAFLIB_STRIP
for extra in ${WAFLIB_STRIP_EXTRAS}
do
    WAFLIB_STRIP="${WAFLIB_STRIP} extras/${extra}.py"
done
for tool in ${WAFLIB_STRIP_TOOLS}
do
    WAFLIB_STRIP="${WAFLIB_STRIP} Tools/${tool}.py"
done

if test -d .git
then
    vcs="git"
fi

dir="${HOME}"/.wafupdate
if ! test -d "${dir}"
then
    mkdir "${dir}" || die "could not create ${dir}"
fi

tardir="${dir}"/${target}
if ! test -d "${tardir}"
then
    mkdir "${tardir}" || die "could not create ${tardir}"
fi

# Fetch the pubkey from the main homepage.
pubkey=pubkey.asc
if ! test -f "${dir}"/${pubkey}
then
    wget https://gitlab.com/ita1024/waf/raw/master/utils/${pubkey} -O "${dir}"/${pubkey} \
        || die "wget failed"
fi

run gpg --import "${dir}"/${pubkey} || die "could not import public key"

wafdir=waf-${target}
waftar=${wafdir}.tar
wafbz2=${waftar}.bz2
wafasc=${wafbz2}.asc

if ! test -f "${tardir}"/${wafbz2}
then
    wget https://waf.io/${wafbz2} -O "${tardir}"/${wafbz2} \
        || die "wget failed"
fi

if ! test -f "${tardir}"/${wafasc}
then
    wget https://waf.io/${wafasc} -O "${tardir}"/${wafasc} \
        || die "wget failed"
fi

run gpg --verify "${tardir}"/${wafasc} "${tardir}"/${wafbz2}
if test $? -eq 0
then
    msg "tarball verification succeeded"
else
    die "tarball verification failed"
fi

if ! test -d "${tardir}"/${wafdir}
then
    tar xjf "${tardir}"/${wafbz2} -C "${tardir}" \
        || die "could not unpack tarball"
fi

# Remove the old waflib directory and replace it with the new.
if test -d waflib
then
    xvcs rm -rf waflib
fi
cp -R "${tardir}"/${wafdir}/waflib . \
    || die "could not copy waflib directory"
xvcs add waflib

msg "stripping waflib"
for s in ${WAFLIB_STRIP}
do
    git rm -f waflib/${s} \
        || die "could not remove tool ${tool}"
done

# Copy the waf-light script to the toplevel directory and rename it to
# waf so that the usual ./waf [args] can be used.
cp "${tardir}"/${wafdir}/waf-light waf || die "could not copy waf-light"
xvcs add waf

exit 0
